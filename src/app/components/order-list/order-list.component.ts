import { Component, OnInit } from '@angular/core';
import { OrderServiceService } from 'src/app/service/order-service.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  countval = 0
  restaurentName: any;
  tableMenuList: any;
  tableCategories: any;
  selectedTab: any;
  quantity: any;
  cartCount: number = 0;
  constructor(private apiService:OrderServiceService) { }

  ngOnInit(): void {
    this.getOrderSectionDetails()
  }

  getOrderSectionDetails(){
    this.apiService.getOrderDetails().subscribe(result=>{
      this.restaurentName   = result[0]["restaurant_name"];
      this.tableMenuList    = result[0]["table_menu_list"];
      this.tableCategories  = result[0]["table_menu_list"][0]["category_dishes"];
      this.tableCategories.map(datas=>{
        datas["count"] = 0;
      })
      this.selectedTab      = this.tableMenuList[0]
    })
  }

  listChanger(listData:any){
    this.tableCategories = listData["category_dishes"]
    this.tableCategories.map(datas=>{
      datas["count"] = 0;
    })
    this.selectedTab = listData
  }
  countSection(count:number,data){
  
    this.tableCategories.map(datas=>{
   
      if(datas.dish_id == data.dish_id){
        this.countval = data.count
        if(this.countval<=0){
          this.countval = 1
        }else{
          this.countval = this.countval + 1     
        }
          datas.count = this.countval  

      }

      
   
      console.log("addcrtcnt",this.cartCount)
    })
    // this.cartCount = this.cartCount + 1
    if(count<=0){
      this.cartCount = 0;
    }else{
      this.cartCount = count
    }
    
  }
   emptyOrder:Boolean=false
   counterNumber
  countSectionMinus(count:number,data){
    
    this.tableCategories.map(datas=>{
      if(datas.dish_id == data.dish_id){
        this.countval = data.count
        if(data<=0){
          this.countval = 0
        }else{
          this.countval = this.countval - 1     
        }
        if(this.countval<=0){
          datas.count = 0
        }else{
          datas.count = this.countval
        }
        this.counterNumber = datas.count
          console.log("values",datas.count,this.countval,count)
         
      }
      if(count<=0 ){
        this.cartCount = 0;
      }else if(this.countval == 0){
        this.cartCount = count;
        return
      }else{
        
        this.cartCount = count
      }
      
    })
    
    
    console.log("log",this.cartCount)
    
    console.log('sdfs',this.counterNumber,this.emptyOrder)
    // if(this.counterNumber==0 && !this.emptyOrder){
    //   this.cartCount = this.cartCount-1
    //   this.emptyOrder=true
    // }else if(this.counterNumber>0){
    //   this.cartCount = count
    // }
    
  }

}
