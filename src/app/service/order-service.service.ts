import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrderServiceService {

  constructor(private http : HttpClient ) { }
  private api_URL = environment.api_URL;
  private headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  };
  private requestOptions = {
    headers: new HttpHeaders(this.headerDict)
  };
  
  // Service Starts--------------------
  getOrderDetails(){
    return this.http.get(environment.api_URL+'v2/5dfccffc310000efc8d2c1ad',this.requestOptions)
  .pipe(map(response=>{
    return response
  }))
  }
}
